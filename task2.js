import { csv } from 'csvtojson'
import { createReadStream, createWriteStream, writeFile, readFile } from 'fs';
import { pipeline } from 'stream';
import { promisify } from 'util';
import { createInterface } from 'readline';


const csvStream = csv();
const readStream = createReadStream('./node_mentoring_t1_2_input_example.csv', 'utf8');
const writeStream = createWriteStream('./node_mentoring_t1_2_input_example.txt', 'utf8');

const write = promisify(writeFile);
const read = promisify(readFile);

read('./node_mentoring_t1_2_input_example.csv', 'utf8')
    .then(data => {
        return csv().fromString(data);
    })
    .then(json => {
        const formatedData = json.map(item => JSON.stringify(item)).join('\n');
        write('./node_mentoring_t1_1_2_input_example.txt', formatedData)
    })
    .catch(err => {
        console.error(`error happen ---> ${err}`)
    });

const readline = createInterface({
    input: readStream,
});

pipeline(
    readline.input,
    csvStream,
    writeStream,
    (err) => {
        if (err) {
            console.log(`error happen ---> ${err}`);
        }
    }
);

// just to log data
readline.on('line', (data) => {
    console.log(`line ---> ${data}`);
});
