function reverseString(str) {
    return str.split("").reverse().join("");
}

process.stdin.setEncoding('utf8');

process.stdin.on('data', (data) => {
    const reversedString = reverseString(data);
    process.stdout.write(`${reversedString} \n`);
});
